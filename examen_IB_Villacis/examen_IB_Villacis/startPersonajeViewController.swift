//
//  startPersonajeViewController.swift
//  examen_IB_Villacis
//
//  Created by andrea villacis on 12/12/17.
//  Copyright © 2017 andrea villacis. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class startPersonajeViewController: UIViewController {

    
    
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var alturaLabel: UILabel!
    @IBOutlet weak var cumpleañosLabel: UILabel!
    @IBOutlet weak var masaLabel: UILabel!
    @IBOutlet weak var generoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func consultarPersonajeButton(_ sender: Any) {
        let swID = arc4random_uniform(10) + 1
        Alamofire.request("https://swapi.co/api/people/\(swID)").responseObject { (response: DataResponse<StartPersonaje>) in
                
         let personaje = response.result.value
         
         DispatchQueue.main.async {
            self.nombreLabel.text = personaje?.nombre ?? ""
            self.alturaLabel.text = personaje?.altura ?? ""
            self.masaLabel.text = personaje?.masa ?? ""
            self.cumpleañosLabel.text = personaje?.cumpleaños ?? ""
            self.generoLabel.text = personaje?.genero ?? ""
            }
         }
        
    }
    
}
