//
//  StartPersonaje.swift
//  examen_IB_Villacis
//
//  Created by andrea villacis on 12/12/17.
//  Copyright © 2017 andrea villacis. All rights reserved.
//

import Foundation
import ObjectMapper

class StartPersonaje: Mappable {
    
    var nombre: String?
    var altura: String?
    var masa: String?
    var cumpleaños: String?
    var genero: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nombre <- map["name"]
        altura <- map["height"]
        masa <- map["mass"]
        cumpleaños <- map["birth_year"]
        genero <- map["gender"]
    }
    
}

