//
//  peliculasWars.swift
//  examen_IB_Villacis
//
//  Created by andrea villacis on 12/12/17.
//  Copyright © 2017 andrea villacis. All rights reserved.
//

import Foundation
import ObjectMapper

class StartPeliculas: Mappable {
    
    var Titulo: String?
    var Episodio: Double?
    var Director: String?
    var Productor: String?
    var FechaLanzamiento: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Titulo <- map["title"]
        Episodio <- map["episode_id"]
        Director <- map["director"]
        Productor <- map["producer"]
        FechaLanzamiento <- map["release_date"]
    }
    
}
