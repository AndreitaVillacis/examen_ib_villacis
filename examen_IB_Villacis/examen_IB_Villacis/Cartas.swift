//
//  Cartas.swift
//  examen_IB_Villacis
//
//  Created by andrea villacis on 13/12/17.
//  Copyright © 2017 andrea villacis. All rights reserved.
//

import Foundation
import ObjectMapper


class Cartas: Mappable {
    
    var estado: String?
    var descripcion: String?
    var tipoCarta: String?
    var tipo: String?
    var familia: String?
    var ataque: Int?
    var defensa: Int?
    var nivel: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        estado <- map["status"]
        descripcion <- map["data.text"]
        tipoCarta <- map["data.card_type"]
        tipo <- map["data.type"]
        familia <- map["data.family"]
        ataque <- map["data.atk"]
        defensa <- map["data.def"]
        nivel <- map["data.level"]
    }
}
