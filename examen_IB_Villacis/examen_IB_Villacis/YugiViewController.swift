//
//  YugiViewController.swift
//  examen_IB_Villacis
//
//  Created by andrea villacis on 13/12/17.
//  Copyright © 2017 andrea villacis. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class YugiViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var personajeLabel: UILabel!
    @IBOutlet weak var personajesPickerView: UIPickerView!
    @IBOutlet weak var DescripcionTextView: UITextView!
    @IBOutlet weak var TipoCartaLabel: UILabel!
    @IBOutlet weak var TipoLabel: UILabel!
    @IBOutlet weak var FamiliaLabel: UILabel!
    @IBOutlet weak var AtaqueLabel: UILabel!
    @IBOutlet weak var DefensaLabel: UILabel!
    @IBOutlet weak var NivelLabel: UILabel!
    
    let personajes = ["copycat","jinzo","kuriboh","Baobaboon","uraby","sangan","boxer","Blocker","Caninetaur","Blockman"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return personajes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return personajes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       personajeLabel.text =  personajes[row]
    }
    
    
    @IBAction func ConsultarButtonPressed(_ sender: Any) {
        
        Alamofire.request("https://yugiohprices.com/api/card_data/\(personajeLabel.text!)").responseObject { (response: DataResponse<Cartas>) in
            
            let carta = response.result.value
            
            DispatchQueue.main.async {
                let carta = response.result.value
                self.DescripcionTextView.text = carta?.descripcion ?? ""
                self.TipoCartaLabel.text = carta?.tipoCarta ?? ""
                print(carta?.tipoCarta ?? "")
                self.TipoLabel.text = carta?.tipo ?? ""
                self.FamiliaLabel.text = carta?.familia ?? ""
                self.AtaqueLabel.text = "\(carta?.ataque ?? 0)"
                self.DefensaLabel.text = "\(carta?.defensa ?? 0)"
                self.NivelLabel.text = "\(carta?.nivel ?? 0)"
            }
            
        }
        
    }
}
