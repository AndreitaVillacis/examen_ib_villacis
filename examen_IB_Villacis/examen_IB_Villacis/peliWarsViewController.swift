//
//  peliWarsViewController.swift
//  examen_IB_Villacis
//
//  Created by andrea villacis on 12/12/17.
//  Copyright © 2017 andrea villacis. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class peliWarsViewController: UIViewController {

    
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var episodioLabel: UILabel!
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var productorLabel: UILabel!
    @IBOutlet weak var fechaLanzamientoLabel: UILabel!
    
    @IBOutlet weak var peliculaTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func consultarPeliculasButton(_ sender: Any) {
        
        Alamofire.request("https://swapi.co/api/films/\(peliculaTextField.text!)").responseObject { (response: DataResponse<StartPeliculas>) in

    
            let pelicula = response.result.value
            
            DispatchQueue.main.async {
                self.tituloLabel.text = pelicula?.Titulo ?? ""
                self.episodioLabel.text = "\(pelicula?.Episodio ?? 0)"
                self.directorLabel.text = pelicula?.Director ?? ""
                self.productorLabel.text = pelicula?.Productor ?? ""
                self.fechaLanzamientoLabel.text = pelicula?.FechaLanzamiento ?? ""
                
            }
        }
        
    
    }
    
   
}
